package UnitTest;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import project3.PuzzleModel;
import project3.SolverModel;

public class SolverModelTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void SolverModelTestConstructorSolution() throws Exception {
		PuzzleModel puzzleModelTest = new PuzzleModel(new File("puzzles/03_easy_puzzle1.txt"));
		SolverModel solverModelTest = new SolverModel(puzzleModelTest);
		assertNotNull(solverModelTest);
		assertNotNull(solverModelTest.getSolvedPuzzleModel());
		System.out.println("move list size: " + solverModelTest.getSolvedPuzzleModel().getMovesList().size());
		System.out.println( solverModelTest.getSolvedPuzzleModel().getMovesList().toString());
	}

	@Test
	public void SolverModelTestConstructorSolutionStress() throws Exception {
		PuzzleModel puzzleModelTest = new PuzzleModel(new File("puzzles/10_special_2d_pieces_proj3m.txt"));
		SolverModel solverModelTest = new SolverModel(puzzleModelTest);
		assertNotNull(solverModelTest);
		assertNotNull(solverModelTest.getSolvedPuzzleModel());
		System.out.println("move list size: " + solverModelTest.getSolvedPuzzleModel().getMovesList().size());
		System.out.println( solverModelTest.getSolvedPuzzleModel().getMovesList().toString());
	}

	
	@Test
	public void SolverModelTestConstructorNoSolution() throws Exception {
		PuzzleModel puzzleModelTest = new PuzzleModel(new File("puzzles/x_unsolvable.txt"));
		SolverModel solverModelTest = new SolverModel(puzzleModelTest);
		assertNotNull(solverModelTest);
		assertNull(solverModelTest.getSolvedPuzzleModel());
	}

}
