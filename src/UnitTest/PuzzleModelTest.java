package UnitTest;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.Point;
import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import project3.PuzzleModel;
import project3.PuzzlePieceModel;

public class PuzzleModelTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void PuzzleModelConstructorTest() throws Exception {
		PuzzleModel PuzzleModelTest = new PuzzleModel(new File("puzzles/01_easy_proj3a.txt"));
		assertEquals(PuzzleModelTest.getPieceList().size(), 8);
		
		for (int i=0; i<PuzzleModelTest.getPieceList().size(); i++)
			assertEquals(PuzzleModelTest.getPieceList().get(i).getPieceType(), PuzzlePieceModel.PieceType.BOTH);
		
		assertEquals(PuzzleModelTest.getPieceList().get(0).getColor(), Color.RED);
		assertEquals(PuzzleModelTest.getPieceList().get(7).getColor(), Color.YELLOW);
		assertEquals(PuzzleModelTest.getPiece(new Point(2,1)), PuzzleModelTest.getPieceList().get(0));
		assertEquals(PuzzleModelTest.getPiece(new Point(3, 3)), PuzzleModelTest.getPieceList().get(7));
	}
	
	@Test
	public void MovePieceTest() throws Exception {
		PuzzleModel PuzzleModelTest = new PuzzleModel(new File("puzzles/01_easy_proj3a.txt"));
		System.out.println("Move Piece Test");
		System.out.println(PuzzleModelTest);
		
		Point source, destination;
		
		source = new Point(0, 1);
		destination = new Point(0, 2);
		assertTrue(PuzzleModelTest.movePiece(source, destination));
		System.out.println(PuzzleModelTest);
		
		source = new Point(0, 2);
		destination = new Point(1, 1);		
		assertFalse(PuzzleModelTest.movePiece(source, destination));
		
		source = new Point(1, 2);
		destination = new Point(1, 1);		
		assertTrue(PuzzleModelTest.movePiece(source, destination));
		System.out.println(PuzzleModelTest);
		
		source = new Point(3, 3);
		destination = new Point(0, 3);
		assertTrue(PuzzleModelTest.movePiece(source, destination));
		System.out.println(PuzzleModelTest);
		
		source = new Point(1, 2);
		destination = new Point(1, 4);		
		assertFalse(PuzzleModelTest.movePiece(source, destination));
		
		source = new Point(1, 2);
		destination = new Point(1, 3);		
		assertTrue(PuzzleModelTest.movePiece(source, destination));
		System.out.println(PuzzleModelTest);				
		
		assertFalse(PuzzleModelTest.isGameFinished());
	}
	
	@Test
	public void PuzzleModelCopyConstructorTest() throws Exception {
		PuzzleModel sourcePuzzle = new PuzzleModel(new File("puzzles/06_hard_proj3f.txt"));
		PuzzleModel copyPuzzle = new PuzzleModel(sourcePuzzle);
		
		assertEquals(copyPuzzle.getBoardHeight(), sourcePuzzle.getBoardHeight());
		assertEquals(copyPuzzle.getBoardWidth(), sourcePuzzle.getBoardWidth());
		assertNotEquals(copyPuzzle.getPieceList(), sourcePuzzle.getPieceList());
		assertEquals(copyPuzzle.getPieceList().size(), sourcePuzzle.getPieceList().size());
		assertNotEquals(copyPuzzle.getPieceList().get(4), sourcePuzzle.getPieceList().get(4));
		assertEquals(copyPuzzle.getPieceList().get(4).getId(), sourcePuzzle.getPieceList().get(4).getId());
		assertEquals(copyPuzzle.toString(), sourcePuzzle.toString());
		assertEquals(copyPuzzle.isGameFinished(), sourcePuzzle.isGameFinished());
		assertEquals(copyPuzzle.getPieceList().get(5).getId(), sourcePuzzle.getPieceList().get(5).getId());
	}
}