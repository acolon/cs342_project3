/**
 * Debug.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */
package project3;

/**
 * This class defines if debug mode is on or off.
 * If ON is set to true, conditional compiling is enabled
 * and code unreachable will not be compiled.
 */
public final class Debug {
	public static final boolean ON = false;
} 