/**
 * MenuController.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */

package project3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * Controller for menus. Controls logic of what happens when menu items are clicked
 */
public class MenuController implements ActionListener{
	private MenuCommand menuCommand;
	private BoardView boardView;

	/**
	 * Possible menu commands
	 */
	public enum MenuCommand {
		OPEN,
		HELP,
		ABOUT,
		EXIT
	}

	/**
	 * Constructor
	 * @param boardView view the controller is associated with
	 * @param menuCommand the menu command
	 */
	public MenuController(BoardView boardView, MenuCommand menuCommand) {
		this.boardView = boardView;
		this.menuCommand = menuCommand;
	}

	/**
	 *  Handler for the action clicking on a menu item.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {		
		switch (menuCommand){
		case EXIT:
			System.exit(0);
			break;
		case HELP:
			JOptionPane.showMessageDialog(boardView, "Welcome to the game of Sliding Block Puzzles!\n\n" +
					"In this game, your goal is to move the red piece labeled 'Z' over to the right side\n" + 
					"of the board such that any part of it is occupied in the the right most column.\n\n" +
					"In order to move a piece, simply drag and drop it to the desired location.", "Help",
					JOptionPane.INFORMATION_MESSAGE);
			break;
		case ABOUT:
			JOptionPane.showMessageDialog(boardView, "CS 342 Project 3 - Sliding Block Puzzles\nSpring 2014\nUniversity of Illinois at Chicago" + 
					"\n\nAuthors:\nAnthony Colon\nacolon8@uic.edu\n\nYonathan Gordon\nygordo2@uic.edu", "About", JOptionPane.INFORMATION_MESSAGE);
			break;
		case OPEN:
			boardView.loadPuzzle(getPuzzleFile(boardView));
		default:
			break;
		}

	}

	/**
	 * This method allows the user to open any puzzle file.
	 * 
	 * @param boardView the Board View to get the file from
	 */
	public static File getPuzzleFile(BoardView boardView) {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File("./puzzles/"));
		int returnVal = fc.showOpenDialog(boardView);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
		    File file = fc.getSelectedFile();
		    if (Debug.ON) {
		    	System.out.println("Opening: " + file.getAbsolutePath());
		    }
		    return file;
		} else {
			if (Debug.ON) {
				System.out.println("Open command cancelled by user.");
			}
		    return null;
		}
	}

}
