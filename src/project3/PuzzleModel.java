/**
 * PuzzleModel.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */

package project3;

import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Stores the Puzzle in a data model and provides methods for manipulating the model. 
 */
public class PuzzleModel {
	private int boardHeight;
	private int boardWidth;
	private int moveCounter;
	private boolean gameFinished;
	private File puzzleFile;
	
	private List<PuzzlePieceModel> pieceList;
	private List<PieceMove> movesList;
	private PuzzlePieceModel board[][];
	
	/**
	 * Empty constructor
	 */
	public PuzzleModel() {};
	
	/**
	 * Constructor from filename
	 * @param filename The file to load into model
	 * @throws Exception Any issues reading file
	 */
	public PuzzleModel(File filename) throws Exception {
		this.puzzleFile = filename;
		this.moveCounter = 0;
		this.pieceList = new ArrayList<PuzzlePieceModel>();
		this.movesList = new LinkedList<PieceMove>();
		
		Scanner scanner = new Scanner(filename);
		
		this.boardHeight = scanner.nextInt();
		this.boardWidth = scanner.nextInt();
		this.gameFinished = false;
		
		this.initBoard();

		String pattern = "^(\\d+\\s+){4}?[hvb]\\n*$";
		while (scanner.hasNext()) {
			String nextLine = scanner.nextLine();
			if (nextLine.matches(pattern)) {
				String splitLine[] = nextLine.split("\\s+");
				PuzzlePieceModel newPiece = new PuzzlePieceModel(
						new Point(Integer.parseInt(splitLine[0]) - 1,
								Integer.parseInt(splitLine[1]) - 1),
								Integer.parseInt(splitLine[2]),
								Integer.parseInt(splitLine[3]),
								splitLine[4], pieceList.size());
				this.placePiece(newPiece);
			}
		}

		scanner.close();
	}
	
	/**
	 * Copy constructor
	 * @param sourcePuzzle The puzzle model to copy
	 * @throws Exception 
	 */
	public PuzzleModel(PuzzleModel sourcePuzzle) throws Exception {
		this.puzzleFile = sourcePuzzle.puzzleFile;
		this.boardHeight = sourcePuzzle.boardHeight;
		this.boardWidth = sourcePuzzle.boardWidth;
		this.moveCounter = sourcePuzzle.moveCounter;
		this.gameFinished = sourcePuzzle.gameFinished;
		
		this.pieceList = new ArrayList<PuzzlePieceModel>();
		this.movesList = new LinkedList<PieceMove>();
		this.initBoard();
		
		for (int k=0; k<sourcePuzzle.pieceList.size(); k++) {
			PuzzlePieceModel newPiece = new PuzzlePieceModel(sourcePuzzle.pieceList.get(k));
			this.placePiece(newPiece);
		}
		
		for (int k=0; k<sourcePuzzle.movesList.size(); k++) {
			PieceMove newMove = new PieceMove(sourcePuzzle.movesList.get(k));
			this.movesList.add(newMove);
		}
	}

	/**
	 * Places a piece in the puzzle model
	 * 
	 * @param newPiece The piece to place
	 * @throws Exception 
	 */
	private void placePiece(PuzzlePieceModel newPiece) throws Exception {
		for (int i=newPiece.getRow(); i<newPiece.getRow() + newPiece.getHeight(); i++) {
			for (int j=newPiece.getCol(); j<newPiece.getCol() + newPiece.getWidth(); j++) {
				if (board[i][j]!=null) throw new Exception();	//invalid board
				board[i][j] = newPiece;
			}
		}		
		this.pieceList.add(newPiece);
	}

	/**
	 * Initializes the board in the data model to be empty
	 */
	private void initBoard() {
		this.board = new PuzzlePieceModel[boardHeight][boardWidth];
		for (int i=0; i<boardHeight; i++) {
			for (int j=0; j<boardWidth; j++) {
				this.board[i][j] = null;
			}
		}
	}
	
	
	/**
	 * Getter for pieceList
	 * @return the pieceList
	 */
	public List<PuzzlePieceModel> getPieceList() {
		return pieceList;
	}

	/**
	 * Setter for movesList
	 * @return the movesList
	 */
	public List<PieceMove> getMovesList() {
		return movesList;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String returnString = new String();
		for (int i=0; i<boardHeight; i++) {
			for (int j=0; j<boardWidth; j++) {
				if (board[i][j]==null)
					returnString = returnString.concat("-" + " ");
				else
					returnString = returnString.concat(board[i][j] + " ");
			}
			returnString = returnString.concat("\n");
		}
		return returnString;
	}
	
	/**
	 * Getter for boardHeight
	 * @return the boardHeight
	 */
	public int getBoardHeight() {
		return boardHeight;
	}

	/**
	 * Getter for boardWidth
	 * @return the boardWidth
	 */
	public int getBoardWidth() {
		return boardWidth;
	}

	/**
	 * Getter for gameFinished
	 * @return the gameFinished
	 */
	public boolean isGameFinished() {
		return gameFinished;
	}
	
	/**
	 * Checks if the game is finished and updates gameFinished
	 * @return true if the game is finished, otherwise false 
	 */
	public boolean checkGameFinished() {
		if (gameFinished) return gameFinished;
		for (int i=0; i<this.boardHeight; i++) {
			if (board[i][this.boardWidth-1] == pieceList.get(0)) {
				gameFinished = true;
				return true;
			}
		}
		return false;
	}

	/**
	 * Getter for moveCounter
	 * @return the moveCounter
	 */
	public int getMoveCounter() {
		return moveCounter;
	}

	/**
	 * Getter for puzzleFile
	 * @return the puzzleFile
	 */
	public File getPuzzleFile() {
		return puzzleFile;
	}

	/**
	 * Gets the piece from board
	 * @param location
	 * @return the puzzle piece
	 */
	public PuzzlePieceModel getPiece(Point location) {
		return board[location.x][location.y];
	}
	
	/**
	 * Moves a puzzle piece if possible. It also checks to see if the move solves
	 * the puzzle.
	 * 
	 * @param source Source
	 * @param destination Destination
	 * @return True if successful
	 */
	public boolean movePiece(Point source, Point destination) {
		if (source.equals(destination)) return false;
		if (getPiece(source) == null) return false;		
		
		PuzzlePieceModel sourcePiece = getPiece(source);
		
		switch (sourcePiece.getPieceType()) {
		case HORIZONTAL:
			if (destination.x!=source.x) return false;
			break;
		case VERTICAL:
			if (destination.y!=source.y) return false;
			break;
		case BOTH:
			if (destination.y!=source.y && destination.x!=source.x) return false;
			break;
		default:
			return false;
		}

		//get piece starting point and use offset to calculate destination starting point
		Point offset = new Point(source.x - sourcePiece.getRow(), source.y - sourcePiece.getCol());		
		source.setLocation(sourcePiece.getRow(), sourcePiece.getCol());
		destination.setLocation(destination.x - offset.x, destination.y - offset.y);
		
		//check bounds
		if (destination.x < 0 || destination.y < 0) return false;
		if (destination.x + sourcePiece.getHeight() > this.boardHeight) return false;
		if (destination.y + sourcePiece.getWidth() > this.boardWidth) return false;
			
		//verify destination is empty
		for (int i=destination.x; i<destination.x + sourcePiece.getHeight(); i++) {
			for (int j=destination.y; j<destination.y + sourcePiece.getWidth(); j++) {
				if (board[i][j]!=null && board[i][j]!=sourcePiece)
					return false;
			}
		}
		
		//verify clear path to destination
		Point pathDifference = new Point (destination.x - source.x, destination.y - source.y);
		int startPathX, stopPathX, startPathY, stopPathY;
		startPathX = (pathDifference.x > 0) ? source.x : destination.x;
		stopPathX = ((pathDifference.x > 0) ? destination.x : source.x) + sourcePiece.getHeight() - 1;
		startPathY = (pathDifference.y > 0) ? source.y : destination.y;
		stopPathY = ((pathDifference.y > 0) ? destination.y : source.y) + sourcePiece.getWidth() - 1;		
		for (int i=startPathX; i<=stopPathX; i++) {
			for (int j=startPathY; j<=stopPathY; j++) {
				if (board[i][j]!=null && board[i][j]!=sourcePiece)
					return false;
			}
		}		

		//destination empty and path is clear -> ok to move piece.
		//remove piece from current position
		for (int i=sourcePiece.getRow(); i<sourcePiece.getRow() + sourcePiece.getHeight(); i++) {
			for (int j=sourcePiece.getCol(); j<sourcePiece.getCol() + sourcePiece.getWidth(); j++) {
				board[i][j] = null;
			}
		}
				
		//set piece to new position
		sourcePiece.setRow(destination.x);
		sourcePiece.setCol(destination.y);
		for (int i=destination.x; i<destination.x + sourcePiece.getHeight(); i++) {
			for (int j=destination.y; j<destination.y + sourcePiece.getWidth(); j++) {
				board[i][j] = sourcePiece;
			}		
		}
		
		this.moveCounter++;
		this.movesList.add(new PieceMove(sourcePiece.getId(), source, destination));
		this.checkGameFinished();
		return true;
	}
}
