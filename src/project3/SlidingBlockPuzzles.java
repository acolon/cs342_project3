/**
 * SlidingBlockPuzzles.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */

package project3;

/**
 * This class will create an instance of the game,
 * and prompt the user to choose a puzzle file.
 */
public class SlidingBlockPuzzles {
	public static void main(String[] args) {
		BoardView application = new BoardView();
		application.loadPuzzle(MenuController.getPuzzleFile(application));
	}
}
