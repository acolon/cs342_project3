/**
 * PuzzlePieceController.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */

package project3;

import java.awt.Color;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;

public class PuzzlePieceController implements DragGestureListener, DragSourceListener, DropTargetListener, Transferable {

	private DragSource ds = DragSource.getDefaultDragSource();
	
	private static Point source;
	private static Point destination;
	
	private int row;
    private int col;
    private BoardView boardView;
    
    private JButton[][] buttons;
    
    /**
     * Constructor sets up location information and provides reference for
     * data model.
     * 
     * @param row Row
     * @param col Column
     * @param boardView The BoardView that will be controlled
     */
    public PuzzlePieceController(int row, int col, BoardView boardView) {
        this.row = row;
        this.col = col;
        this.boardView = boardView;
        this.buttons = boardView.getButtons();
        
        int action = DnDConstants.ACTION_MOVE;
		ds.createDefaultDragGestureRecognizer(buttons[row][col], action, this);
        
    }
	
    /**
     * Detects if drag started and marks the source point
     */
	@Override
	public void dragGestureRecognized(DragGestureEvent dge) {
		if (boardView.getPuzzle().isGameFinished()) return;
		try
		{
			if (Debug.ON) {
				System.out.println("Start drag from " + row + ", " + col);
			}
			source = new Point(row, col);
			dge.startDrag (DragSource.DefaultMoveDrop, this, this);
		}
		catch (InvalidDnDOperationException e2)
		{
			if (Debug.ON) {
				System.out.println (e2);
			}
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.dnd.DropTargetListener#drop(java.awt.dnd.DropTargetDropEvent)
	 */
	@Override
	public void drop(DropTargetDropEvent dtde) {
		if (boardView.getPuzzle().isGameFinished()) return;
		if (Debug.ON) {
			System.out.println("Dropped on button " + row + " , " + col);
		}
		destination = new Point(row, col);
		
		if (!boardView.getPuzzle().movePiece(source, destination)) return; 
		if (Debug.ON) {
			System.out.println(boardView.getPuzzle());
		}
		boardView.setSolver(null);
		updateBoard(boardView);
	}

	/**
	 * This method will update all the colors on the board and check the game state to see
	 * if the puzzle was solved.
	 */
	public static void updateBoard(BoardView boardView) {
		boardView.setNumMovesLabel(boardView.getPuzzle().getMoveCounter());		
		for (int i=0; i<boardView.getPuzzle().getBoardHeight(); i++) {
			for (int j=0; j<boardView.getPuzzle().getBoardWidth(); j++) {
				Point location = new Point(i, j);
				if (boardView.getPuzzle().getPiece(location) != null) {
					boardView.getButtons()[i][j].setBackground(boardView.getPuzzle().getPiece(location).getColor());
					boardView.getButtons()[i][j].setText(boardView.getPuzzle().getPiece(location).toString());
				}
				else {
					boardView.getButtons()[i][j].setBackground(Color.WHITE);
					boardView.getButtons()[i][j].setText(null);
				}
				boardView.getButtons()[i][j].validate();
			}
		}
		boardView.validate();
		boardView.repaint();
		
		if (boardView.getPuzzle().isGameFinished()) {
			JOptionPane.showMessageDialog(boardView, "You solved the puzzle in " + 
					boardView.getPuzzle().getMoveCounter() + " moves!\n\n" +
					"Thank you for playing this puzzle.", "Finished",
					JOptionPane.INFORMATION_MESSAGE);
			loadNextPuzzleFile(boardView);
		}
	}

	/**
	 * Loads the next puzzle file in sequence
	 * @param boardView provides reference for current puzzle file
	 */
	private static void loadNextPuzzleFile(BoardView boardView) {
		File f = boardView.getPuzzle().getPuzzleFile();
		List<File> FileList = new ArrayList<File>(Arrays.asList(f.getParentFile().listFiles()));
		int index = FileList.indexOf(f);
		if (index!=-1 && index<FileList.size()-1) {
			File nextPuzzle = FileList.get(index+1);
			if (nextPuzzle.getName().toLowerCase().contains("x_unsolvable")) return;	//do not auto load unsolvable file
			boardView.loadPuzzle(nextPuzzle);
		}
	}

	@Override
	public void dragEnter(DragSourceDragEvent dsde) {}


	@Override
	public void dragOver(DragSourceDragEvent dsde) {}


	@Override
	public void dropActionChanged(DragSourceDragEvent dsde) {}


	@Override
	public void dragExit(DragSourceEvent dse) {}


	@Override
	public void dragDropEnd(DragSourceDropEvent dsde) {}


	@Override
	public void dragEnter(DropTargetDragEvent dtde) {}


	@Override
	public void dragOver(DropTargetDragEvent dtde) {}


	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {}

	@Override
	public void dragExit(DropTargetEvent dte) {}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return null;
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return false;
	}

	@Override
	public Object getTransferData(DataFlavor flavor)
			throws UnsupportedFlavorException, IOException {
		return null;
	}
}
