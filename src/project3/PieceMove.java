/**
 * PieceMove.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */

package project3;

import java.awt.Point;

/**
 * PieceMove class stores a move, that is a source and destination for a given
 * piece id.
 * 
 * This class is an immutable class.
 */
public class PieceMove {
	private int id;
	private Point source;
	private Point destination;				
	
	/**
	 * Constructor for PieceMove sets id, source, and destination.
	 * @param id piece id
	 * @param source move source
	 * @param destination move destination
	 */
	public PieceMove(int id, Point source, Point destination) {
		this.id = id;
		this.source = source;
		this.destination = destination;
	}
	
	/**
	 * Copy constructor for PieceMove
	 * @param pieceMove the PieceMove to copy 
	 */
	public PieceMove(PieceMove pieceMove) {
		this.id = pieceMove.id;
		this.source = pieceMove.source;
		this.destination = pieceMove.destination;
	}
	/**
	 * Getter for id
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Getter for source
	 * @return the source
	 */
	public Point getSource() {
		return source;
	}
	
	/**
	 * Getter for destination
	 * @return the destination
	 */
	public Point getDestination() {
		return destination;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PieceMove [id=");
		builder.append(id);
		builder.append(", source=");
		builder.append(source);
		builder.append(", destination=");
		builder.append(destination);
		builder.append("]");
		return builder.toString();
	}
}