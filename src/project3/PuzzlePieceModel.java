/**
 * PuzzlePieceModel.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */

package project3;

import java.awt.Color;
import java.awt.Point;
import java.util.Random;

/**
 * This class contains all the characteristics of a puzzle piece as a data model.
 */
public class PuzzlePieceModel {
	private int row, col, width, height, id;
	private PieceType pieceType;
	private Color color;
	
	private static final Color[] colorArray = {Color.RED, Color.BLUE, Color.CYAN, Color.GREEN, Color.ORANGE, Color.PINK, Color.MAGENTA, Color.YELLOW };
	
	/**
	 * Define all possible piece types
	 */
	public static enum PieceType {
		 HORIZONTAL,
		 VERTICAL,
		 BOTH
	}
	
	/**
	 * Constructor for puzzle piece
	 * @param location coordinates of piece
	 * @param width width of piece
	 * @param height Height of piece
	 * @param moveType Piece movement type
	 * @param id piece identifier
	 */
	public PuzzlePieceModel(Point location, int width, int height, String moveType, int id) {
		this.row = location.x;
		this.col = location.y;
		this.width = width;
		this.height = height;
		this.id = id;
		
		if (id < colorArray.length) {
			this.color = colorArray[id];
		} else {
			Random rand = new Random();		//ran out of colors so make a random color
			this.color = new Color(rand.nextInt(0xFFFFFF));
		}
		
		switch (moveType.toLowerCase().charAt(0)) {
		case 'h':
			this.pieceType = PieceType.HORIZONTAL;
			break;
		case 'v':
			this.pieceType = PieceType.VERTICAL;
			break;
		default:
			this.pieceType = PieceType.BOTH;
		}
	}

	/**
	 * Copy constructor for PuzzlePieceModel
	 * @param sourcePiece the piece to copy
	 */
	public PuzzlePieceModel(PuzzlePieceModel sourcePiece) {
		this.row = sourcePiece.row;
		this.col = sourcePiece.col;
		this.width = sourcePiece.width;
		this.height = sourcePiece.height;
		this.id = sourcePiece.id;
		this.pieceType = sourcePiece.pieceType;
		this.color = sourcePiece.color;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (id!=0)
			return Integer.toString(id);
		else
			return "Z";
	}

	/**
	 * Getter for row
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Setter for row
	 * @param row the row to set
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * Getter for col
	 * @return the column
	 */
	public int getCol() {
		return col;
	}

	/**
	 * Setter for col
	 * @param col the col to set
	 */
	public void setCol(int col) {
		this.col = col;
	}

	/**
	 * Getter for width
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Getter for height
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Getter for id
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter for pieceType
	 * @return the pieceType
	 */
	public PieceType getPieceType() {
		return pieceType;
	}

	/**
	 * Getter for Color
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}
}