/**
 * TopButtonController.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */
package project3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Provides a controller for the buttons on top of the board view.
 */
public class TopButtonController implements ActionListener {
	private ButtonCommand ButtonCommand;
	private BoardView boardView;

	/**
	 * Possible menu commands
	 */
	public enum ButtonCommand {
		RESET,
		SOLVE,
		HINT
	}

	/**
	 * Constructor
	 * 
	 * @param boardView view the controller is associated with
	 * @param ButtonCommand the menu command
	 */
	public TopButtonController(BoardView boardView, ButtonCommand ButtonCommand) {
		this.boardView = boardView;
		this.ButtonCommand = ButtonCommand;
	}

	/**
	 *  Handler for clicking on button
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (ButtonCommand){
		case RESET:
			resetBoard();
			break;
		case SOLVE:
			solveBoard();
			break;
		case HINT:
			giveHint();
			break;
		default:
			break;
		}

	}
	
	/**
	 * Resets the puzzle board by clearing it and reloading the puzzle file.
	 */
	private void resetBoard() {
		if (boardView.getPuzzle()==null) return;
		File resetFile = new File(boardView.getPuzzle().getPuzzleFile().getAbsolutePath());
		boardView.disposeBoard();
		boardView.loadPuzzle(resetFile);
	}

	/**
	 * Provides a one move hint to the user by accessing the SolverModel.
	 */
	private void giveHint() {
		if (!this.hasSolver()) return;
		PieceMove nextMove = boardView.getSolver().getSolvedPuzzleModel().getMovesList().remove(0);
		boardView.getPuzzle().movePiece(nextMove.getSource(), nextMove.getDestination());				
		PuzzlePieceController.updateBoard(boardView);
	}

	/**
	 * Displays all the moves needed to solve the board in an animation by accessing
	 * the SolverModel.
	 */
	private void solveBoard() {
		if (!this.hasSolver()) return;
		synchronized (boardView) {
			Thread t = new Thread() {
				public void run() {
					Queue<PieceMove> solverMoveQueue =
							new LinkedList<PieceMove>(boardView.getSolver().getSolvedPuzzleModel().getMovesList());
					boardView.setEnabled(false);
					while (!solverMoveQueue.isEmpty()) {
						PieceMove nextMove = solverMoveQueue.remove();
						boardView.getPuzzle().movePiece(nextMove.getSource(), nextMove.getDestination());				
						PuzzlePieceController.updateBoard(boardView);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {}
					}
					boardView.setEnabled(true);
				}
			};
			t.start();
		}
	}

	/**
	 * Checks solver if it is current.
	 * If it is not current, it will create a new solver.
	 * @return if has valid solver
	 */
	private boolean hasSolver() {
		if (boardView.getPuzzle()==null || boardView.getPuzzle().isGameFinished())
			return false;
		
		if (boardView.getSolver()==null) {
			try {
				boardView.setSolver(new SolverModel(boardView.getPuzzle()));
			} catch (Exception e1) {
				if (Debug.ON) {
					e1.printStackTrace();
				}
				return false;
			}
		}
		
		return true;
	}

}
