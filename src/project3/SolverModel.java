/**
 * SolverModel.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */

package project3;

import java.awt.Point;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import project3.PuzzlePieceModel.PieceType;

/**
 * This class provides a data model for solving a given puzzle
 * using breadth first search.
 */
public class SolverModel {
	private PuzzleModel givenPuzzleModel;
	private PuzzleModel solvedPuzzleModel;
	
    private Map<String,String> solverMap;
    private Queue<PuzzleModel> solverQueue;
    
    /**
     * Constructor for SolverModel
     * 
     * @param givenPuzzleModel The puzzle to solve.
     * @throws Exception Invalid puzzle
     */
	public SolverModel(PuzzleModel givenPuzzleModel) throws Exception {
		this.givenPuzzleModel = new PuzzleModel(givenPuzzleModel);
		this.solverMap = new HashMap<String, String>();
		this.solverQueue = new LinkedList<PuzzleModel>();
		this.givenPuzzleModel.getMovesList().clear();
		this.solvePuzzle();
	}
	
	/**
	 * Solves the puzzling using breadth first search.
	 * 
	 * @throws Exception Invalid puzzle
	 */
	public void solvePuzzle() throws Exception {
		addEdge(givenPuzzleModel, new PuzzleModel());
        
		while (!solverQueue.isEmpty()) {
            PuzzleModel currentModel = solverQueue.remove();
            if (currentModel.checkGameFinished()) {
            	this.solvedPuzzleModel = currentModel;
                break;
            }
            explorePuzzle(currentModel);
        }
        
		if (Debug.ON) {
			System.out.println(solverMap.size() + " puzzle models examined.");
			if (this.solvedPuzzleModel==null)
				System.out.println("no solution found.");
			else
				System.out.println(solvedPuzzleModel.toString());
		}
	}

	/**
	 * Adds an edge to the graph
	 * 
	 * @param nextModel Destination of edge
	 * @param prevModel Source of edge
	 */
    private void addEdge(PuzzleModel nextModel, PuzzleModel prevModel) {
    	if (solverMap.containsKey(nextModel.toString())) return; 
    	solverMap.put(nextModel.toString(), prevModel.toString());
    	solverQueue.add(nextModel);
    }
    
    /**
     * Explores all movements of each piece in a puzzle
     * 
     * @param currentModel The puzzle to explore
     * @throws Exception Invalid puzzle
     */
    private void explorePuzzle(PuzzleModel currentModel) throws Exception {
    	for (PuzzlePieceModel puzzlePiece : currentModel.getPieceList()) {
    		explorePuzzlePiece(currentModel, puzzlePiece);
    	}    	
    }

    /**
     * Explores all possible movements of a given piece in a given puzzle model
     * and add a new Edge if the resulting puzzle model has not been explored.
     * 
     * @param currentModel The puzzle to explore
     * @param puzzlePiece The piece to explore
     * @throws Exception Invalid puzzle
     */
	private void explorePuzzlePiece(PuzzleModel currentModel, PuzzlePieceModel puzzlePiece) throws Exception {		
		PieceType puzzlePieceType = puzzlePiece.getPieceType();
		
		if (puzzlePieceType==PieceType.VERTICAL || puzzlePieceType==PieceType.BOTH) {
			for (int i=puzzlePiece.getRow(); i>=0; i--) {
				PuzzleModel nextModel = new PuzzleModel(currentModel);
				Point source = new Point(puzzlePiece.getRow(), puzzlePiece.getCol());
				Point destination = new Point(puzzlePiece.getRow() - i, puzzlePiece.getCol());
				if (nextModel.movePiece(source, destination))
					addEdge(nextModel, currentModel);
			}	
			for (int i=0; i<currentModel.getBoardHeight() - puzzlePiece.getRow(); i++) {
				PuzzleModel nextModel = new PuzzleModel(currentModel);
				Point source = new Point(puzzlePiece.getRow(), puzzlePiece.getCol());
				Point destination = new Point(puzzlePiece.getRow() + i, puzzlePiece.getCol());
				if (nextModel.movePiece(source, destination))
					addEdge(nextModel, currentModel);
			}	
		}
		
		if (puzzlePieceType==PieceType.HORIZONTAL || puzzlePieceType==PieceType.BOTH) {
			for (int i=puzzlePiece.getCol(); i>=0; i--) {
				PuzzleModel nextModel = new PuzzleModel(currentModel);
				Point source = new Point(puzzlePiece.getRow(), puzzlePiece.getCol());
				Point destination = new Point(puzzlePiece.getRow(), puzzlePiece.getCol() - i);
				if (nextModel.movePiece(source, destination))
					addEdge(nextModel, currentModel);
			}	
			for (int i=0; i<currentModel.getBoardWidth() - puzzlePiece.getCol(); i++) {
				PuzzleModel nextModel = new PuzzleModel(currentModel);
				Point source = new Point(puzzlePiece.getRow(), puzzlePiece.getCol());
				Point destination = new Point(puzzlePiece.getRow(), puzzlePiece.getCol() + i);
				if (nextModel.movePiece(source, destination))
					addEdge(nextModel, currentModel);
			}	
		}
	}

	/**
	 * Getter for solved PuzzleModel
	 * @return the solvedPuzzleModel
	 */
	public PuzzleModel getSolvedPuzzleModel() {
		return solvedPuzzleModel;
	}
}
