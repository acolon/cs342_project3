/**
 * BoardView.java
 * CS 342 Project 3 - Sliding Block Puzzles
 */
package project3;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

/**
 * BoardView is the view for the game. It provides all GUI aspects of the game. 
 */
public class BoardView extends JFrame {
	private static final long serialVersionUID = 3083089738846709863L;
	private static final String boardTitle = "Sliding Block Puzzles";
	private static final String numMovesText = "Current: ";
	private static final String minMovesText = "Minimum: ";

	private JButton buttons[][];  
	private GridLayout puzzleGridLayout;
	private PuzzleModel puzzle;
	private SolverModel solver;

	private JPanel topPanel = new JPanel();
	private JPanel centerPanel = new JPanel();

	private JButton resetButton;
	private JButton hintButton;
	private JButton solveButton;

	private JLabel numMovesLabel;
	private JLabel minMovesLabel;

	private Container container;

	/**
	 * This is the constructor for the class.
	 * The GUI for the game is initialized - container, panels, labels, and buttons are created.
	 */
	public BoardView()
	{
		super(boardTitle);
		puzzle = null;
		solver = null;

		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e) {
			if (Debug.ON) {
				e.printStackTrace();
			}
		}		

		resetButton = new JButton("Reset");
		hintButton = new JButton("Hint");
		solveButton= new JButton("Solve");

		numMovesLabel = new JLabel();
		minMovesLabel = new JLabel();
		numMovesLabel.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));

		resetButton.addActionListener(new TopButtonController(this, TopButtonController.ButtonCommand.RESET));
		hintButton.addActionListener(new TopButtonController(this, TopButtonController.ButtonCommand.HINT));
		solveButton.addActionListener(new TopButtonController(this, TopButtonController.ButtonCommand.SOLVE));

		topPanel.setLayout(new GridLayout(2, 3, 4, 4));
		topPanel.add(resetButton);
		topPanel.add(hintButton);
		topPanel.add(solveButton);

		topPanel.add(numMovesLabel);
		topPanel.add(minMovesLabel);

		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(topPanel,BorderLayout.NORTH);

		container.add(centerPanel,BorderLayout.CENTER);
		createDropDownMenu();
		setSize(600, 600);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * In this method we load a puzzle from a file and display it.
	 * If the puzzle is invalid or unsolvable an exception is thrown and the
	 * current puzzle stays loaded.
	 *  
	 * @param puzzleFile the file to load.
	 */
	public void loadPuzzle(File puzzleFile) {
		if (puzzleFile==null) return;
		PuzzleModel newPuzzle;
		SolverModel newSolver;
		try {
			this.setEnabled(false);
			newPuzzle = new PuzzleModel(puzzleFile);
			newSolver = new SolverModel(newPuzzle);
		} catch (Exception e) {
			if (Debug.ON) { 
				System.err.println("Error opening \"" + puzzleFile.getName() + "\"file");
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(this, "The file \"" + puzzleFile.getName() + "\" could not be opened.\n\nPlease check the following:\n" +
					"- The file is correctly formatted (see support documentation)\n" + 
					"- The puzzle is valid (no overlapping pieces)",
					"Can't open file!", JOptionPane.ERROR_MESSAGE);
			this.setEnabled(true);
			return;
		}

		if (newSolver.getSolvedPuzzleModel() == null) {
			JOptionPane.showMessageDialog(this, "The puzzle \"" + puzzleFile.getName() + "\" is NOT solvable.",
					"Unsolvable Puzzle", JOptionPane.ERROR_MESSAGE);
			this.setEnabled(true);
			return;
		} else if (newPuzzle.checkGameFinished() == true) {
			JOptionPane.showMessageDialog(this, "The puzzle \"" + puzzleFile.getName() + "\" is already solved.",
					"Solved Puzzle", JOptionPane.ERROR_MESSAGE);			
			this.setEnabled(true);
			return;
		}

		this.disposeBoard();
		this.puzzle = newPuzzle;
		this.solver = newSolver;
		this.puzzleGridLayout = new GridLayout(puzzle.getBoardHeight(), puzzle.getBoardWidth());
		this.centerPanel.setLayout(puzzleGridLayout);
		this.setSize(75*puzzle.getBoardWidth(), 
				75*puzzle.getBoardHeight() + topPanel.getHeight());
		this.createPiecesView();
		this.setTitle(boardTitle + " - " + puzzleFile.getName());
		this.numMovesLabel.setText(numMovesText + puzzle.getMoveCounter());
		this.minMovesLabel.setText(minMovesText + solver.getSolvedPuzzleModel().getMovesList().size());

		this.validate();
		this.repaint();
		this.setEnabled(true);
	}

	/**
	 * This method initializes all the buttons on the board.
	 * It also assigns a puzzle piece controller to each button.  
	 */
	private void createPiecesView()
	{
		buttons = new JButton[puzzle.getBoardHeight()][puzzle.getBoardWidth()];
		for (int i=0; i<puzzle.getBoardHeight(); i++) {
			for (int j=0; j<puzzle.getBoardWidth(); j++) {
				buttons[i][j] = new JButton();
				PuzzlePieceController ppc = new PuzzlePieceController(i, j, this);				
				buttons[i][j].setFocusPainted(false);
				buttons[i][j].setDropTarget(new DropTarget(buttons[i][j], DnDConstants.ACTION_MOVE, ppc)); 
				centerPanel.add(buttons[i][j]);
			}
		}
		PuzzlePieceController.updateBoard(this);
	}

	/**
	 * Adds all menu items and listeners to the menu
	 */
	private void createDropDownMenu()
	{
		JMenu fileMenu = new JMenu("File");
		JMenu helpMenu = new JMenu("Help");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		helpMenu.setMnemonic(KeyEvent.VK_H);

		JMenuItem openItem = new JMenuItem("Open...", KeyEvent.VK_O);
		JMenuItem helpItem = new JMenuItem("Help", KeyEvent.VK_H);
		JMenuItem aboutItem = new JMenuItem("About", KeyEvent.VK_A);
		JMenuItem exitItem = new JMenuItem("Exit", KeyEvent.VK_X);

		openItem.addActionListener(new MenuController(this, MenuController.MenuCommand.OPEN));
		helpItem.addActionListener(new MenuController(this, MenuController.MenuCommand.HELP));
		aboutItem.addActionListener(new MenuController(this, MenuController.MenuCommand.ABOUT));
		exitItem.addActionListener(new MenuController(this, MenuController.MenuCommand.EXIT));

		fileMenu.add(openItem);
		fileMenu.addSeparator();
		fileMenu.add(exitItem);

		helpMenu.add(helpItem);
		helpMenu.addSeparator();
		helpMenu.add(aboutItem);

		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
	}

	/**
	 * Getter for buttons
	 * @return the buttons
	 */
	public JButton[][] getButtons() {
		return buttons;
	}

	/**
	 * Getter for PuzzleModel
	 * @return the puzzle
	 */
	public PuzzleModel getPuzzle() {
		return puzzle;
	}

	/**
	 * @param puzzle the puzzle to set
	 */
	public void setPuzzle(PuzzleModel puzzle) {
		this.puzzle = puzzle;
	}

	/**
	 * Sets the current move label
	 * @param count current move
	 */
	public void setNumMovesLabel(int count) {
		this.numMovesLabel.setText(numMovesText + count);
	}

	/**
	 * Getter for solver
	 * @return the solver
	 */
	public SolverModel getSolver() {
		return solver;
	}

	/**
	 * Setter for solver
	 * @param solver the solver to set
	 */
	public void setSolver(SolverModel solver) {
		this.solver = solver;
	}

	/**
	 * Unloads and clears the current board
	 */
	public void disposeBoard() {
		if (puzzle==null) return;
		for (int i=0; i<puzzle.getBoardHeight(); i++) {
			for (int j=0; j<puzzle.getBoardWidth(); j++) {
				buttons[i][j].setDropTarget(null);
				centerPanel.remove(buttons[i][j]);
			}
		}
		puzzle = null;
	}
} 
